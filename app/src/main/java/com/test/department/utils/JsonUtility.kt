package com.test.department.utils

import android.content.Context
import com.google.gson.Gson
import java.io.IOException
import java.io.InputStream

object JsonUtility {
    fun Context.loadJSONFromAsset(pFileName: String): String? {
        return try {
            val `is`: InputStream = assets.open(pFileName)
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
    }

    fun getAssetModelData(pJsonContent: String?, pClass: Class<*>): Any? {
        return try {
            Gson().fromJson(pJsonContent, pClass)
        } catch (ex: IOException) {
            ex.printStackTrace()
            null
        }
    }
}