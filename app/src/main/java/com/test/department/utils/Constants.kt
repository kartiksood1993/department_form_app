package com.test.department.utils

object Constants {
    const val sSpinner = "Spinner"
    const val sEditText = "edit"
    const val sButton = "button"
    const val sMatchParent = "match_parent"
    const val sHorizontal = "horizontal"
}