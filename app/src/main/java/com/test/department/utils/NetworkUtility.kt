package com.test.department.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.test.department.R
import okhttp3.Interceptor
import okhttp3.Response

class NetworkUtility(private val applicationContext: Context?) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isInternetAvailable())
            throw NoInternetException(
               applicationContext?.getString(R.string.check_internet_connection)
            )
        try {
            return chain.proceed(chain.request())
        } catch (exception: Exception) {
            throw NoServerFoundException(exception.message)
        }
    }


    private fun isInternetAvailable(): Boolean {
        var result = false
        val connectivityManager =
            applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        connectivityManager?.let {
            it.getNetworkCapabilities(connectivityManager.activeNetwork)?.apply {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    else -> false
                }
            }
        }
        return result
    }
}