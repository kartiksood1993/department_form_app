package com.test.department.utils

import java.io.IOException

class NoInternetException(message: String?) : IOException(message)
class NoServerFoundException(message: String?) : IOException(message)