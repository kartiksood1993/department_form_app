package com.test.department.application

import android.app.Application
import com.test.department.dagger.ApplicationComponent
import com.test.department.dagger.ApplicationModule
import com.test.department.dagger.DaggerApplicationComponent

class DepartmentApplication : Application() {
    companion object {
        private var applicationComponent: ApplicationComponent? = null

        fun getApplicationComponent(): ApplicationComponent? {
            return applicationComponent
        }

        fun setApplicationComponent(applicationComponent: ApplicationComponent?) {
            this.applicationComponent = applicationComponent
        }
    }

    override fun onCreate() {
        super.onCreate()

        setApplicationComponent(
            DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
        )
    }
}