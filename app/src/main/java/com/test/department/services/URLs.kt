package com.test.department.services

object URLs {
    object Retrofit {
        const val sContentType = "Content-type"
        const val sContentTypeJson = "application/json"
        var sAPI_BASE_URL = "https://cloud.vgo.global:8000/"
        const val sTime = 10
    }
}