package com.test.department.services

import com.test.department.application.DepartmentApplication
import com.test.department.utils.NetworkUtility
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BaseService @Inject internal constructor(
    pApplication: DepartmentApplication?
) {

    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(URLs.Retrofit.sTime.toLong(), TimeUnit.SECONDS)
        .readTimeout(URLs.Retrofit.sTime.toLong(), TimeUnit.SECONDS)
        .writeTimeout(URLs.Retrofit.sTime.toLong(), TimeUnit.SECONDS)
        .addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .addHeader(URLs.Retrofit.sContentType, URLs.Retrofit.sContentTypeJson)
                .method(original.method(), original.body())
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        .addInterceptor(
            NetworkUtility(
                pApplication?.applicationContext
            )
        )


    private fun createOkHttpClient(): OkHttpClient = okHttpClient.build()

    fun getClient(): API {
        return Retrofit.Builder()
            .baseUrl( URLs.Retrofit.sAPI_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(createOkHttpClient())
            .build()
            .create(API::class.java)
    }
}