package com.test.department.services

import com.test.department.modules.dashboard.models.response.LocationDataResponse
import com.test.department.modules.dashboard.models.response.ObjectResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface API {
    @POST
    fun request(@Url url: String?, @Body pRequest: Any?): Completable

    @GET
    fun getObjectResponse(@Url url: String): Single<ObjectResponse>

    @GET
    fun getLocationDataResponse(@Url url: String): Single<LocationDataResponse>
}