package com.test.department.dagger

import com.test.department.application.DepartmentApplication
import com.test.department.modules.dashboard.DashboardActivity
import com.test.department.services.BaseService
import dagger.Component

@Component(modules = [ApplicationModule::class])
@ApplicationScope
interface ApplicationComponent {
    fun getApplicationInstance(): DepartmentApplication
    fun provideBaseService(): BaseService

    fun inject(dashboardActivity: DashboardActivity)
}