package com.test.department.dagger

import com.test.department.application.DepartmentApplication
import com.test.department.services.BaseService
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: DepartmentApplication) {
    @Provides
    @ApplicationScope
    fun providesApplication(): DepartmentApplication {
        return application
    }

    @Provides
    @ApplicationScope
    fun provideBaseService(): BaseService {
        return BaseService(application)
    }
}