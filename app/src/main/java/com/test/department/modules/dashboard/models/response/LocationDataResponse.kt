package com.test.department.modules.dashboard.models.response

import com.google.gson.annotations.SerializedName

class LocationDataResponse(@SerializedName("locations") val location: ArrayList<Location>)

class Location(val code: String, val location: String)