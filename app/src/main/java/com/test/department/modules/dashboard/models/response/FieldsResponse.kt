package com.test.department.modules.dashboard.models.response

import com.google.gson.annotations.SerializedName

class FieldsResponse(
    val id: String,
    val widgetType: String,
    val keyboard: String,
    val placeholder: String,
    val required: String,
    val label: String,
    val options: String?,
    val textAlignment: String,
    val singleLine: String,
    val width: Int,
    val height: Int,
    val marginLeft: String,
    val marginRight: String,
    val marginTop: String,
    val marginBottom: String,
    val txtColor: String,
    val textSize: String,
    val textStyle: String,
    val visibility: Boolean,
    val padding: String,
    val gravity: String,
    @SerializedName("onAction") val onActionResponse: OnActionResponse?
)

class OnActionResponse(
    val event: String,
    val serviceType: String,
    val uri: String,
    @SerializedName("mapFields") val mapFieldResponse: ArrayList<MapFieldResponse>
)

class MapFieldResponse(val name: String)