package com.test.department.modules.dashboard

import android.annotation.SuppressLint
import android.content.res.Resources
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.test.department.R
import com.test.department.application.DepartmentApplication
import com.test.department.databinding.ActivityDashboardBinding
import com.test.department.modules.dashboard.models.response.*
import com.test.department.utils.Constants
import com.test.department.utils.JsonUtility
import com.test.department.utils.JsonUtility.loadJSONFromAsset
import javax.inject.Inject


class DashboardActivity : AppCompatActivity() {

    @Inject
    lateinit var dashboardViewModel: DashboardViewModel

    lateinit var activityDashboardBinding: ActivityDashboardBinding
    private lateinit var locationDataResponse: LocationDataResponse
    private var locationsNames = ArrayList<String>()
    private val hashMap = HashMap<String, String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DepartmentApplication.getApplicationComponent()?.inject(this)
        activityDashboardBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        activityDashboardBinding.activity = this
        activityDashboardBinding.viewmodel = dashboardViewModel
        getDepartmentData()
    }

    private fun getDepartmentData() {
//        dashboardViewModel.getDepartmentsData()
//        dashboardViewModel.getDepartmentDataResponse().observe(this, Observer {
        val it: ObjectResponse? = JsonUtility.getAssetModelData(
            loadJSONFromAsset("Departments.json"),
            ObjectResponse::class.java
        ) as? ObjectResponse
        val dynamicLayoutResponse = it?.listDynamicLayoutResponse!!
        for (index in 0 until dynamicLayoutResponse.size) {
            val departmentResponse = dynamicLayoutResponse[index].departmentResponse
            val layout = createLayoutConfiguration(departmentResponse)
            val fieldsResponse = departmentResponse.fieldsResponse
            for (fieldIndex in 0 until fieldsResponse.size) {
                val fieldsResponseData = fieldsResponse[fieldIndex]
                when (fieldsResponseData.widgetType) {
                    Constants.sSpinner -> {
                        val spinner = createSpinnerConfiguration(fieldsResponseData)
                        layout.addView(spinner)
                    }
                    Constants.sEditText -> {
                        val editText = createEditTextConfiguration(fieldsResponseData)
                        layout.addView(editText)
                    }
                }
            }
            layout.addView(createButtonConfigurationAndHandleClickListener(departmentResponse.submitToResponse))
        }
        //})
    }


    private fun createLayoutConfiguration(departmentResponse: DepartmentResponse): LinearLayout {
        val parent = LinearLayout(this)
        val width = if (departmentResponse.width == Constants.sMatchParent) {
            LinearLayout.LayoutParams.MATCH_PARENT
        } else {
            LinearLayout.LayoutParams.WRAP_CONTENT
        }
        val height = if (departmentResponse.height == Constants.sMatchParent) {
            LinearLayout.LayoutParams.MATCH_PARENT
        } else {
            LinearLayout.LayoutParams.WRAP_CONTENT
        }
        val orientation = if (departmentResponse.orientation == Constants.sHorizontal) {
            LinearLayout.HORIZONTAL
        } else {
            LinearLayout.VERTICAL
        }
        parent.layoutParams = LinearLayout.LayoutParams(width, height)
        parent.orientation = orientation
        activityDashboardBinding.container.addView(parent)
        return parent
    }

    private fun createEditTextConfiguration(fieldsResponse: FieldsResponse): EditText {
        val editText = EditText(this)
        editText.layoutParams =
            ViewGroup.LayoutParams(dpToPx(fieldsResponse.width), dpToPx(fieldsResponse.height))
        editText.hint = fieldsResponse.placeholder
        editText.tag = fieldsResponse.id.plus("##").plus(Constants.sEditText).plus("##")
            .plus(fieldsResponse.required)
        editText.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, fieldsResponse.textSize.replace("sp", "").toFloat(),
            resources.displayMetrics
        )
        editText.gravity = when (fieldsResponse.textAlignment) {
            "textStart" -> {
                Gravity.START
            }
            "textEnd" -> {
                Gravity.END
            }
            "textCenter" -> {
                Gravity.CENTER
            }
            else -> {
                Gravity.START
            }
        }
        return editText
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun createSpinnerConfiguration(fieldsResponse: FieldsResponse): Spinner {
        val spinner = Spinner(this)
        spinner.tag = fieldsResponse.id.plus("##").plus(Constants.sSpinner).plus("##")
            .plus(fieldsResponse.required)
        spinner.layoutParams =
            ViewGroup.LayoutParams(dpToPx(fieldsResponse.width), dpToPx(fieldsResponse.height))
        if (fieldsResponse.options != null) {
            val options = ArrayList<String>()
            options.add(getString(R.string.empty))
            options.addAll(fieldsResponse.options.split("|"))
            val spinnerArrayAdapter: ArrayAdapter<String> = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                options
            )
            spinner.adapter = spinnerArrayAdapter
        } else if (fieldsResponse.onActionResponse != null) {
            locationsNames.add(getString(R.string.empty))
            val spinnerArrayAdapter: ArrayAdapter<String> = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                locationsNames
            )
            spinner.adapter = spinnerArrayAdapter
            val onAction = fieldsResponse.onActionResponse
            if (onAction.event == "onFocus") {
                spinner.setOnTouchListener { _, event ->
                    if (event.action == MotionEvent.ACTION_UP) {
                        dashboardViewModel.getLocationData(onAction.uri)
                        if (!::locationDataResponse.isInitialized) {
                            getLocationDataResponse(spinner)
                        }
                    }
                    false
                }
            }
        }
        return spinner
    }

    private fun getLocationDataResponse(spinner: Spinner) {
        dashboardViewModel.getLocationDataResponse().observe(this, Observer { dataResponse ->
            locationDataResponse = dataResponse
            locationsNames.clear()
            locationsNames.add(getString(R.string.empty))
            locationsNames.addAll(locationDataResponse.location.map { it.location })
            val spinnerArrayAdapter: ArrayAdapter<String> = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                locationsNames
            )
            spinner.adapter = spinnerArrayAdapter
        })
    }

    private fun createButtonConfigurationAndHandleClickListener(submitToResponse: SubmitToResponse): AppCompatButton {
        val button = AppCompatButton(this)
        button.text = getString(R.string.submit)
        button.tag =
            submitToResponse.importResponse.type.plus("##").plus(Constants.sButton).plus("##")
                .plus("true")
        var validDataEnter: Boolean
        button.setOnClickListener {
            validDataEnter = true
            val count = activityDashboardBinding.container.childCount
            if (count > 0) {
                val layoutView = activityDashboardBinding.container.getChildAt(1) as LinearLayout
                val childCount = layoutView.childCount
                if (childCount > 0) {
                    loop@ for (index in 0 until childCount) {
                        val view = layoutView.getChildAt(index)
                        val tag = view.tag.toString().split("##")
                        val name = tag[0]
                        val type = tag[1]
                        val required = tag[2]
                        if (submitToResponse.export.size > 0 && submitToResponse.export[0].contains(
                                name
                            )
                        ) {
                            when (type) {
                                Constants.sEditText -> {
                                    val editText = view as EditText
                                    if (editText.text.toString()
                                            .isEmpty() && required == "true"
                                    ) {
                                        showToast(getString(R.string.fields_empty))
                                        validDataEnter = false
                                        break@loop
                                    } else {
                                        hashMap[name] = editText.text.toString()
                                    }
                                }
                                Constants.sSpinner -> {
                                    val spinner = view as Spinner
                                    if (spinner.selectedItem.toString() == "Empty" && required == "true"
                                    ) {
                                        showToast(getString(R.string.fields_empty))
                                        validDataEnter = false
                                        break@loop
                                    } else {
                                        hashMap[name] = spinner.selectedItem.toString()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (validDataEnter) {
                dashboardViewModel.sendSelectedDepartmentData(submitToResponse.uri, hashMap)
            }
        }
        return button
    }

    private fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

    private fun showToast(message: String) {
        Toast.makeText(
            this,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }
}