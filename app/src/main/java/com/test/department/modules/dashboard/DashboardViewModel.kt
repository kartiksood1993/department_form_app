package com.test.department.modules.dashboard

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.test.department.application.DepartmentApplication
import com.test.department.modules.dashboard.models.response.LocationDataResponse
import com.test.department.modules.dashboard.models.response.ObjectResponse
import com.test.department.services.BaseService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DashboardViewModel @Inject internal constructor(
    application: DepartmentApplication,
    baseService: BaseService
) : AndroidViewModel(application) {

    val isBusy = ObservableBoolean()
    private val api = baseService.getClient()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val objectResponse = MutableLiveData<ObjectResponse>()
    private val locationDataResponse = MutableLiveData<LocationDataResponse>()

    fun getDepartmentsData() {
        compositeDisposable.add(
            api.getObjectResponse(".")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { ObjectResponse(ArrayList()) }
                .doOnError { objectResponse.value = ObjectResponse(ArrayList()) }
                .doOnSuccess { objectResponse.value = it }
                .subscribe())
    }

    fun getDepartmentDataResponse(): MutableLiveData<ObjectResponse> = objectResponse

    fun getLocationData(uri: String) {
        compositeDisposable.add(
            api.getLocationDataResponse(uri)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isBusy.set(true) }
                .onErrorReturn { LocationDataResponse(ArrayList()) }
                .doOnError {
                    isBusy.set(false)
                    locationDataResponse.value = LocationDataResponse(ArrayList())
                }
                .doOnSuccess {
                    isBusy.set(false)
                    locationDataResponse.value = it
                }
                .subscribe())
    }

    fun getLocationDataResponse(): MutableLiveData<LocationDataResponse> = locationDataResponse

    fun sendSelectedDepartmentData(uri: String, hashMap: HashMap<String, String>) {
        val body = Gson().toJson(hashMap)
        isBusy.set(true)
        compositeDisposable.add(
            api.request(uri, body)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { isBusy.set(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ isBusy.set(false) }, { isBusy.set(false) })
        )
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}