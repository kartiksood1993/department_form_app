package com.test.department.modules.dashboard.models.response

import com.google.gson.annotations.SerializedName

class DepartmentResponse(
    val type: String,
    val name: String,
    val width: String,
    val height: String,
    val orientation: String,
    val background: String,
    val padding: String,
    val margin: String,
    @SerializedName("submitTo") val submitToResponse: SubmitToResponse,
    @SerializedName("fields") val fieldsResponse: ArrayList<FieldsResponse>
)


class SubmitToResponse(
    val type: String,
    @SerializedName("export:") val export: ArrayList<String>,
    val uri: String,
    @SerializedName("import") val importResponse: ImportResponse
)

class ImportResponse(val type: String, val export: ArrayList<String>, val uri: String)

