package com.test.department.modules.dashboard.models.response

import com.google.gson.annotations.SerializedName

data class ObjectResponse(@SerializedName("dynlayouts") val listDynamicLayoutResponse: ArrayList<DynamicLayoutResponse>)

class DynamicLayoutResponse(@SerializedName("department") val departmentResponse: DepartmentResponse)